(ns i-am-a-horse-in-the-land-of-booleans
  (:refer-clojure :exclude [boolean]))

(if "foo" "truthy" "falsey") ;=> "truthy"
(if 0     "truthy" "falsey") ;=> "truthy"
(if []    "truthy" "falsey") ;=> "truthy"
(if false "truthy" "falsey") ;=> "falsey"
(if nil   "truthy" "falsey") ;=> "falsey"


(boolean "foo") ;=> true
(boolean nil) ;=> false
(boolean (+ 2 3)) ;=> true
(boolean true) ;=> true
(boolean false) ;=> false

(= "foo" "foo")    ;=> true
(= "foo" "bar")    ;=> false
(< 1 2)   ;=> true
(> 1 2)   ;=> false
(<= 52 2) ;=> false


(teen? 12) ;=> false
(teen? 13) ;=> true
(teen? 15) ;=> true
(teen? 19) ;=> true
(teen? 20) ;=> false
(teen? 27) ;=> false

(defn sign [x]
  (if (< x 0)
    "-"
    "+"))
(sign  2) ;=> "+"
(sign -2) ;=> "-"
(sign  0) ;=> "+"

(if true
  42
  (/ 1 0))


(abs -2) ;=> 2
(abs 42) ;=> 42

(mod 10 5) ;=> 0
(mod 3 2)  ;=> 1
(divides? 2 4) ;=> true
(divides? 4 2) ;=> false
(divides? 5 10) ;=> true
(divides? 2 5) ;=> false

(defn super-sign [number]
  (cond
    (neg? number) "negative"
    (pos? number) "positive"
    :else         "zero"))

(super-sign 13) ;=> "positive"
(super-sign 0)  ;=> "zero"
(super-sign -5) ;=> "negative"

(fizzbuzz 2)  ;=> ""
(fizzbuzz 45) ;=> "gotcha!"
(fizzbuzz 48) ;=> "fizz"
(fizzbuzz 70) ;=> "buzz"

(generic-doublificate 1)        ;=> 2
(generic-doublificate [1 2])    ;=> 4
(generic-doublificate '(65 21)) ;=> 4
(generic-doublificate {})       ;=> nil
(generic-doublificate [])       ;=> nil
(generic-doublificate {:a 1})   ;=> true

(and true true)   ;=> true
(and true false)  ;=> false

(or  true false)  ;=> true
(or  false false) ;=> false

(not true)        ;=> false
(not false)       ;=> true

(and true)                      ;=> true
(and true true true)            ;=> true
(and true true true true false) ;=> false
(and)                           ;=> true

(or false false false false true) ;=> true
(or false false false)            ;=> false
(or)                              ;=> nil

(and "foo" "bar")   ;=> "bar"
(and "foo"  false)  ;=> false
(and  10    nil)    ;=> nil

(or  "foo"  false)  ;=> "foo"
(or   42    true)   ;=> 42

(not "foo")         ;=> false
(not nil)           ;=> true
(not [])            ;=> false

(not-teen? 13) ;=> false
(not-teen? 25) ;=> true
(not-teen? 12) ;=> true
(not-teen? 19) ;=> false
(not-teen? 20) ;=> true

(leap-year? 100) ;=> false
(leap-year? 200) ;=> false
(leap-year? 400) ;=> true
(leap-year? 12)  ;=> true
(leap-year? 20)  ;=> true
(leap-year? 15)  ;=> false